
import java.util.Scanner;

public class brewCalculator {

	
	public static void main(String[] args) {
		 
		int userChoice;
		 
		 Scanner input = new Scanner(System.in);
		 //Calculator check = new Calculator();
		 //ABOVE used to create new instance of calculator class
		 //Possibly how brewBuddy should be structured with each calculator
		 //in a separate class
		 
		 System.out.println("Select the function: ");
		 System.out.println("Press (1) to Calculate ABV");
		 System.out.println("Press (2) to Calculate IBU");
		 System.out.println("Press (3) for Dillution");
		 System.out.println("Press (4) for Common Conversion(English, Metric)");
		 userChoice = input.nextInt();
		 
		 input.close();
		 
		 switch (userChoice) {
case 1:  abvCalculator();
                  break;
         case 2:  ibuCalculator();
                  break;
         case 3:  dilCalculator();
                  break;
         case 4:  metricConversions();
         		  break;
 
		 }

	}



		private static void abvCalculator() {
			float originalGravity;
			float finalGravity;
			float abv;
			
			System.out.println("brewBuddy ABV Calculator ");
			System.out.println ("\nCalculates alcohol by volume based on gravity change. If you took an original (OG) " +
								"\nAND final (FG) gravity reading prior to adding priming sugar at bottling brewBuddy " +
								"\nwill calculate your batch's alcohol by volume (ABV).");
						
			Scanner inputGravity = new Scanner(System.in);
	
			System.out.println("\nEnter the original gravity(OG) reading: ");
			originalGravity = inputGravity.nextFloat();
			
			System.out.println("\nEnter the final gravity(FG) reading: ");
			finalGravity = inputGravity.nextFloat();
			
			inputGravity.close();
			
			abv = (float) ((76.08 * (originalGravity-finalGravity) / (1.775-originalGravity)) * (finalGravity / 0.794));
			
			System.out.println("\nAlcohol by Volume: " + abv + "%");
			
			System.out.println ("\nLegal Disclaimer: The brewBuddy ABV Calculator is for entertainment purposes and should" +
								"\nnot be used for professional brewing. No warranty or guarantee of accuracy is provided on " +
								"\nthe information provided by this calculator.");
			 
		}

		private static void ibuCalculator() {
			// method to calculate IBU
			
		}

		private static void dilCalculator() {
			// method to calculate ABV
			
		}
		private static void metricConversions() {
			// method to convert fahrenheit to celsius, gallons to liters, pounds to kilograms
			
		}
}
	
	