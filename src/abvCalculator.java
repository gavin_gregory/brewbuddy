import java.util.Scanner;


public class abvCalculator {
	float originalGravity;
	float finalGravity;
	float abv;
	
	System.out.println("brewBuddy ABV Calculator ");
	System.out.println ("\nCalculates alcohol by volume based on gravity change. If you took an original (OG) " +
						"\nAND final (FG) gravity reading prior to adding priming sugar at bottling brewBuddy " +
						"\nwill calculate your batch's alcohol by volume (ABV).");
				
	Scanner inputGravity = new Scanner(System.in);

	System.out.println("\nEnter the original gravity(OG) reading: ");
	originalGravity = inputGravity.nextFloat();
	
	System.out.println("\nEnter the final gravity(FG) reading: ");
	finalGravity = inputGravity.nextFloat();
	
	inputGravity.close();
	
	abv = (float) ((76.08 * (originalGravity-finalGravity) / (1.775-originalGravity)) * (finalGravity / 0.794));
	
	System.out.println("\nAlcohol by Volume: " + abv + "%");
	
	System.out.println ("\nLegal Disclaimer: The brewBuddy ABV Calculator is for entertainment purposes and should" +
						"\nnot be used for professional brewing. No warranty or guarantee of accuracy is provided on " +
						"\nthe information provided by this calculator.");
	 
}
}
