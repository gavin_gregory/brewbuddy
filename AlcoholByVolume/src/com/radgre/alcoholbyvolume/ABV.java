package com.radgre.alcoholbyvolume;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ABV extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_abv);
		
		final EditText editOG = (EditText) findViewById(R.id.editOG);
		final EditText editFG = (EditText) findViewById(R.id.editFG);
		final EditText editResult = (EditText) findViewById(R.id.editResult);
		Button buttonABV = (Button) findViewById(R.id.buttonABV);
		
		buttonABV.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				double originalGravity = Double.valueOf(editOG.getText().toString());
				double finalGravity = Double.valueOf(editFG.getText().toString());
				double result = ((originalGravity - finalGravity) * 131.25);
				editResult.setText(String.valueOf(result));
				
			}
		});
		
	}


}
